package com.cp.androidlibrary.Utilities.SQLLite.Annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by CarlosPinto on 12/06/2017.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Column {
    String name() default "";

    boolean unique() default false;

    boolean nullable() default false;

    String default_value() default "";
}
