package com.cp.androidlibrary.Utilities.Connectivity;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by CarlosPinto on 11/06/2017.
 */

public class Connectivity {


    public boolean isInternetConnected() throws InterruptedException, IOException
    {
        String command = "ping -i 5 -c 1 google.com";
        return (Runtime.getRuntime().exec (command).waitFor() == 0);
    }

 }
