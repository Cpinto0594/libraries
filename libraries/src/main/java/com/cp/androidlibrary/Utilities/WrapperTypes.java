package com.cp.androidlibrary.Utilities;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Created by CarlosP on 15/6/2017.
 */

public abstract class WrapperTypes<T> {
    private Class<?> type_;
    private Class<?> rawType_;

    protected WrapperTypes() {
        this.type_ = getWrapperClass(getClass());
        this.rawType_= getRawClass(getClass());
    }
    public static Class<?> getWrapperClass(Class<?> clase){
        Type type = clase.getGenericSuperclass();
        if (type instanceof Class<?>) {
            return (Class<?>) type;
        }
        ParameterizedType pType = ((ParameterizedType) type);

        if ( pType.getActualTypeArguments()[0] instanceof  ParameterizedType ){
            pType = (ParameterizedType) pType.getActualTypeArguments()[0];
        }
        if (pType.getActualTypeArguments()[0] instanceof Class<?>) {
            return (Class<?>) pType.getActualTypeArguments()[0];
        }
        return null;
    }

    public static Class<?> getRawClass(Class<?> clase){
        Type type = clase.getGenericSuperclass();
        if (type instanceof Class<?>) {
            return null;
        }
        ParameterizedType pType = ((ParameterizedType) type);

        if ( pType.getActualTypeArguments()[0] instanceof  ParameterizedType ){
            pType = (ParameterizedType) pType.getActualTypeArguments()[0];
        }
        if (pType.getRawType() instanceof Class<?>) {
            return (Class<?>) pType.getRawType();
        }
        return null;
    }
    public Class<?> getType_() {
        return type_;
    }

    public Class<?> getRawType_() {
        return rawType_;
    }
}
