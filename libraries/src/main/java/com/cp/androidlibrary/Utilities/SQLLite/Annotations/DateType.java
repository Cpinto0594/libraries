package com.cp.androidlibrary.Utilities.SQLLite.Annotations;

import com.cp.androidlibrary.Utilities.SQLLite.EntityManager;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by carlospinto on 7/11/17.
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DateType {

    String format() default EntityManager.DATEFORMATNOTIME;
}
