package com.cp.androidlibrary.Utilities.SQLLite;

import android.content.Context;

import com.cp.androidlibrary.Utilities.WrapperTypes;


/**
 * Created by CarlosPinto on 11/06/2017.
 */

public abstract class EntityManagerFactory<T> {


    protected EntityManagerFactory() {

    }

    public static  <T extends Object> EntityManager<T> getInstance(Context context){
        return new EntityManagerFactory<T>(){}.getIntance(context);
    }

    public EntityManager<T> getIntance(Context context) {
        try {
            Class<T> clazz = (Class<T>) WrapperTypes.getWrapperClass(this.getClass());
            return new EntityManager<T>(context, clazz);
        } catch (Exception e) {
            return null;
        }
    }

    public static void HOla(){

    }


}
