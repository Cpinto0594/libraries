package com.cp.androidlibrary.Utilities.SQLLite;

import android.database.sqlite.SQLiteDatabase;

import java.io.Serializable;
import java.util.List;

/**
 * Created by CarlosPinto on 12/06/2017.
 */

public interface ISQLEntityManager<T> {
    int Save();

    int SaveOrUpdate();

    int Update();

    int Delete();

    List<T> getAll();

    T getById(Serializable id);

    List<T> getBy(String[] columns, String[] values);

    int getCount();

    int getCount(String[] cols, String[] values);

    //Other entities
    int Save(Object entity);

    int SaveOrUpdate(Object entity);

    int Update(Object entity);

    int Delete(Object entity);

    List<?> getAll(Class<?> entityClass);

    Object getById(Class<?> entityClass, Serializable id);

    List<?> getBy(Class<?> entityClass, String[] columns, String[] values);

    <T extends Object> List<T> getByQuery(Class<T> entityClass, String sqlQuery);

    int getCount(Class<?> entityClass);

    int getCount(Class<?> entityClass, String[] cols, String[] values);

    void ExecuteQuery(String query);

    //Database
    SQLiteDatabase getDatabase();

    void deleteDatabase();

    void Begin();

    void Commit();

    void End();
}
