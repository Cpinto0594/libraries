package com.cp.androidlibrary.Utilities.Http.headers;

import java.util.Date;

/**
 * Created by carlospinto on 26/10/17.
 */

public class HttpCookies {


        private String key;
        private String value;
        private String path;
        private String expires;
        private boolean httpOnly;

        public HttpCookies() {
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getExpires() {
            return expires;
        }

        public void setExpires(String expires) {
            this.expires = expires;
        }

        public boolean isHttpOnly() {
            return httpOnly;
        }

        public void setHttpOnly(boolean httpOnly) {
            this.httpOnly = httpOnly;
        }

    }
