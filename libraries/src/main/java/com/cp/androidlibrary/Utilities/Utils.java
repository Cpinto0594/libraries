package com.cp.androidlibrary.Utilities;


import java.io.BufferedReader;
import java.io.IOException;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created by CarlosP on 17/5/2017.
 */
/*
*
* Metodos Utiles
* */
public class Utils {

    public static Object isNullorEmpty(Object value, Object ifNUll) {
        if (value == null) return ifNUll;
        if (value instanceof String && ((String) value).isEmpty()) {
            return ifNUll;
        }
        return value;
    }

    public static Object ITtrueThenElse(boolean trueExp, Object then, Object elsE) {
        if (trueExp) return then;
        else return elsE;
    }

    public static String getUUiD() {
        return UUID.randomUUID().toString();
    }

    public static boolean isEmpty(Object value) {
        if (value == null) {
            return true;
        }
        if (value instanceof String && ((String) value).trim().isEmpty()) {
            return true;
        }
        if (value instanceof Number && ((Integer) value) == 0) {
            return true;
        }
        return false;
    }

    public static Thread TimeOut(final int delay, final TimeOutListener timeListener, final Object... args) {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    throw new RuntimeException("Error deteniendo el Hilo del TimeOut");
                }
                if (!Utils.isEmpty(timeListener)) {
                    timeListener.onExecute(args);
                }
            }
        });
    }

    public static void WriteErrorLog(String message, Class<?> parent){
        Logger log = Logger.getLogger(parent.getSimpleName());
        log.warning(message);
    }

    public static String frombufferReader(BufferedReader stream) throws IOException {
        if (isEmpty(stream)) return null;

        String output;
        StringBuilder sb = new StringBuilder();
        while ((output = stream.readLine()) != null) {
            //System.out.println(output);
            sb.append(output);
        }
        stream.close();
        return sb.toString();
    }

    public static boolean isPrimitiveOrPrimitiveWrapperOrString(Class<?> type) {
        return (type.isPrimitive() && type != void.class) ||
                type == Double.class || type == Float.class || type == Long.class ||
                type == Integer.class || type == Short.class || type == Character.class ||
                type == Byte.class || type == Boolean.class || type == String.class;
    }


    public static String constructStackTrace(Exception e ){
        Exception aux = e.getCause() != null ? (Exception) e.getCause() : e;
        StringBuilder sb = new StringBuilder();
        sb.append("Exception: ").append(aux.getLocalizedMessage()).append("\n");
        for ( StackTraceElement stack : aux.getStackTrace() ){
            sb.append(" Exception File: ").append(stack.getFileName())
                    .append(" Exception Class: ").append(stack.getClassName())
                    .append(" Exception Method: ").append(stack.getMethodName())
                    .append(" Exception LineNumber: ").append(stack.getLineNumber())
                    .append("\n");
        }
        return sb.toString();
    }

}
