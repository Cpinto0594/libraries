package com.cp.androidlibrary.Utilities;



/**
 * Created by CarlosP on 5/6/2017.
 */

public interface TimeOutListener {

    public void onExecute(Object... args);

}
