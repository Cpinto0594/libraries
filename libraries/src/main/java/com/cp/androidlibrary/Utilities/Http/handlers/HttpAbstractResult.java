package com.cp.androidlibrary.Utilities.Http.handlers;

import com.cp.androidlibrary.Utilities.Http.headers.HttpHeaders;
import com.cp.androidlibrary.Utilities.WrapperTypes;
//import com.fasterxml.jackson.core.type.TypeReference;

import java.util.HashMap;


/**
 * Created by CarlosP on 24/5/2017.
 */

public abstract class HttpAbstractResult<Result> implements HttpResultListeners<Result> {

    protected HttpAbstractResult() {
    }


    @Override
    public boolean onBeforeSend(HashMap<String, String> requestHheaders) {
        return true;
    }

    @Override
    public void onError(Exception excp, HttpHeaders responseHeaders) {
        throw new RuntimeException(excp);
    }

    @Override
    public void onSuccess(Result resultValue, HttpHeaders responseHeaders, int responseCode) {

    }
}
