package com.cp.androidlibrary.Utilities.SQLLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cp.androidlibrary.Utilities.SQLLite.Annotations.Column;
import com.cp.androidlibrary.Utilities.SQLLite.Annotations.DateType;
import com.cp.androidlibrary.Utilities.SQLLite.Annotations.Id;
import com.cp.androidlibrary.Utilities.SQLLite.Annotations.ManyToOne;
import com.cp.androidlibrary.Utilities.SQLLite.Annotations.OneToMany;
import com.cp.androidlibrary.Utilities.Utils;

import junit.framework.Assert;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CarlosPinto on 17/06/2017.
 */

public class EntityManager<T> extends SQLiteOpenHelper implements ISQLEntityManager<T> {
    public static String DATABASENAME = "TestDB.db";
    private static int DATABASEVERSION = 1;
    public static final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATEFORMATNOTIME = "yyyy/MM/dd";
    public static final String TAG = "EntityManager";

    private SQLiteDatabase database;
    private Context context_;
    private String TABLENAME;
    private List<Map<String, Object>> COLUMNAMES;
    private Class<?> CLAZZ;
    private T ENTITY;


    public EntityManager(Context context, Class<T> clazz) {
        super(context, DATABASENAME, null, DATABASEVERSION);
        try {
            this.CLAZZ = clazz;
            this.TABLENAME = clazz.getSimpleName();
            this.context_ = context;
            this.COLUMNAMES = InitEntityColumnVariables(this.CLAZZ);
            if (this.COLUMNAMES == null || this.COLUMNAMES.size() <= 0) {
                throw new RuntimeException("No SQL columns found in this Entity " + this.TABLENAME);
            }

            Log.d(TAG, "Iniciando database " + DATABASENAME + " para entidad: " + this.TABLENAME);
            onCreate(getDatabase());

        } catch (Exception e) {
            Log.e(TAG, getCause(e));
        }
    }

    public EntityManager withEntity(T entity) {
        this.ENTITY = entity;
        return this;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        onCreate_(db, this.CLAZZ, this.TABLENAME);
    }

    private void onCreate_(SQLiteDatabase db, Class<?> clazz, String tablename) {
        try {
            Cursor c = db.rawQuery("select * from sqlite_master  where tbl_name  = '" + tablename + "'", null);
            Log.d("SQLLITE", c.getCount() + "COUNT TABLES");
            if (c != null && c.getCount() > 0) return;

            String sqlCreate = SQLCreateEntityStatement(clazz);
            db.execSQL(sqlCreate);
            Log.d(TAG, "Creando entidad " + tablename + " -> " + sqlCreate);
        } catch (Exception e) {
            Utils.WriteErrorLog(String.format("Couldn´t create table instance %s , %s", tablename, e.getMessage()), getClass());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + this.TABLENAME);
        onCreate(db);
    }

    @Override
    public SQLiteDatabase getDatabase() {
        if (this.database == null || !this.database.isOpen()) {
            this.database = getWritableDatabase();
        }
        return this.database;
    }

    @Override
    public void deleteDatabase() {
        Assert.assertNotNull(this.context_);
        this.context_.deleteDatabase(this.getDatabaseName());
    }

    @Override
    public void Begin() {
        if (this.database == null || !this.database.isOpen()) {
            this.database = getDatabase();
        } else if (this.database.inTransaction()) {
            return;
        }
        this.database.beginTransaction();
    }

    @Override
    public void Commit() {
        if (this.database == null || !this.database.inTransaction()) {
            return;
        }
        this.database.setTransactionSuccessful();
    }

    @Override
    public void End() {
        if (this.database == null || !this.database.inTransaction()) {
            return;
        }
        this.database.endTransaction();
    }

    @Override
    public int Save() {
        return save(this.CLAZZ, this.ENTITY);
    }

    @Override
    public int SaveOrUpdate() {
        Assert.assertNotNull(this.ENTITY);
        try {

            List<Field> fields = fieldsWithAnnotation(this.CLAZZ, Id.class, true);
            if (fields == null || fields.size() <= 0) {
                Utils.WriteErrorLog("No key field found in entity " + this.TABLENAME, EntityManager.class);
                return -1;
            }
            Field f = fields.get(0);
            Object value = f.get(this.ENTITY);
            T entityAux = (T) getById(this.CLAZZ, value.toString());
            if (entityAux == null) {
                return save(this.CLAZZ, this.ENTITY);
            } else {
                return update(this.CLAZZ, this.ENTITY);
            }

        } catch (Exception e) {
            Log.e(TAG, getCause(e));
        }
        return 0;
    }

    @Override
    public int SaveOrUpdate(Object entity) {
        Assert.assertNotNull(entity);
        try {
            onCreate_(getWritableDatabase(), entity.getClass(), entity.getClass().getSimpleName());

            List<Field> fields = fieldsWithAnnotation(entity.getClass(), Id.class, true);
            if (fields == null || fields.size() <= 0) {
                Utils.WriteErrorLog("No key field found in entity " + entity.getClass().getSimpleName(), EntityManager.class);
                return -1;
            }
            Field f = fields.get(0);
            Object value = f.get(entity);
            T entityAux = (T) getById(entity.getClass(), value.toString());
            if (entityAux == null) {
                return save(entity.getClass(), entity);
            } else {
                return update(entity.getClass(), entity);
            }


        } catch (Exception e) {
            Log.e(TAG, getCause(e));
        }
        return 0;
    }

    @Override
    public int Update() {
        return update(this.CLAZZ, this.ENTITY);
    }

    @Override
    public int Delete() {
        Assert.assertNotNull(this.ENTITY);
        try {

            List<Field> fields = fieldsWithAnnotation(this.CLAZZ, Id.class, true);
            if (fields == null || fields.size() <= 0) return 0;
            Field f = fields.get(0);
            Object value = f.get(this.ENTITY);
            String colName = f.getName();
            Annotation annotation = f.getAnnotation(Column.class);
            if (annotation != null && !((Column) annotation).name().isEmpty()) {
                colName = ((Column) annotation).name();
            }
            if (value == null) return 0;

            return delete(this.CLAZZ, this.ENTITY, new String[]{colName}, new String[]{value.toString()});

        } catch (Exception e) {
            Log.e(TAG, getCause(e));
        }
        return 0;
    }

    @Override
    public List<T> getAll() {
        return (List<T>) get(this.CLAZZ, null, null, null, null);
    }

    @Override
    public T getById(Serializable id) {
        Assert.assertNotNull(this.CLAZZ);
        List<Field> field = fieldsWithAnnotation(this.CLAZZ, Id.class, false);
        if (field == null) return null;
        Field f = field.get(0);
        String colName = f.getName();
        Annotation annotation = f.getAnnotation(Column.class);
        if (annotation != null && !((Column) annotation).name().isEmpty()) {
            colName = ((Column) annotation).name();
        }
        String where = colName;
        T entity = (T) getBy((Class<T>) this.CLAZZ, new String[]{where}, new String[]{id.toString()}, null, null);
        return entity;
    }


    @Override
    public int Save(Object entity) {
        onCreate_(this.getWritableDatabase(), entity.getClass(), entity.getClass().getSimpleName());
        return save(entity.getClass(), entity);
    }

    @Override
    public int Update(Object entity) {
        onCreate_(this.getWritableDatabase(), entity.getClass(), entity.getClass().getSimpleName());
        return update(entity.getClass(), entity);
    }

    @Override
    public int Delete(Object entity) {
        Assert.assertNotNull(entity);
        int result = 0;
        try {

            List<Field> fields = fieldsWithAnnotation(entity.getClass(), Id.class, true);
            if (fields == null || fields.size() <= 0) return 0;
            Field f = fields.get(0);
            String colName = f.getName();
            Annotation annotation = f.getAnnotation(Column.class);
            if (annotation != null && !((Column) annotation).name().isEmpty()) {
                colName = ((Column) annotation).name();
            }
            Object value = f.get(entity);
            if (value == null) return 0;
            result = delete(entity.getClass(), entity, new String[]{colName}, new String[]{value.toString()});

        } catch (Exception e) {
            Log.e(TAG, getCause(e));
        }
        return result;
    }

    @Override
    public List<?> getAll(Class<?> entityClass) {
        return get(entityClass, null, null, null, null);
    }

    @Override
    public Object getById(Class<?> entityClass, Serializable id) {
        Assert.assertNotNull(entityClass);
        List<Field> field = fieldsWithAnnotation(entityClass, Id.class, false);
        if (field == null) return null;
        Field f = field.get(0);
        String colName = f.getName();
        Annotation annotation = f.getAnnotation(Column.class);
        if (annotation != null && !((Column) annotation).name().isEmpty()) {
            colName = ((Column) annotation).name();
        }
        List<?> resultList = getBy(entityClass, new String[]{colName}, new String[]{id.toString()}, null, null);

        if (resultList != null && !resultList.isEmpty())
            return resultList.get(0);
        else return null;
    }

    @Override
    public List<?> getBy(Class<?> entityClass, String[] columns, String[] values) {
        return getBy(entityClass, columns, values, null, null);
    }


    @Override
    public List<T> getBy(String[] columns, String[] values) {
        return (List<T>) getBy(this.CLAZZ, columns, values, null, null);
    }


    @Override
    public int getCount(String[] cols, String[] values) {
        return count(this.CLAZZ, cols, values, null, null);
    }

    @Override
    public int getCount(Class<?> entityClass, String[] cols, String[] values) {
        return count(entityClass, cols, values, null, null);
    }

    @Override
    public void ExecuteQuery(String query) {
        Assert.assertNotNull(query);
        getDatabase().execSQL(query);
    }

    @Override
    public int getCount() {
        return count(this.CLAZZ, null, null, null, null);
    }

    @Override
    public int getCount(Class<?> entityClass) {
        return count(entityClass, null, null, null, null);
    }

    @Override
    public <T extends Object> List<T> getByQuery(Class<T> entityClass, String sqlQuery) {
        return getbyquery(entityClass, sqlQuery);
    }

    //Region util methods
    //CRUD
    private int save(Class<?> entityClass, Object entity) {
        Assert.assertNotNull(entity);
        Assert.assertNotNull(entityClass);
        int result = 0;
        try {

            String tableName = entityClass.getSimpleName();

            ContentValues values = getContentValues(entityClass, entity, false);
            result = (int) getDatabase().insertOrThrow(tableName, null, values);

            List<Field> field = fieldsWithAnnotation(entityClass, Id.class, true);
            if (field != null) {
                Field f = field.get(0);
                f.set(entity, result);
            }
            if (!this.database.inTransaction()) {
                this.database.close();
            }


        } catch (Exception e) {
            Log.e(TAG, getCause(e));

        }
        return result;
    }

    private int update(Class<?> entityClass, Object entity) {
        Assert.assertNotNull(entity);
        Assert.assertNotNull(entityClass);
        int result = 0;
        try {

            String tableName = entityClass.getSimpleName();
            ContentValues values = getContentValues(entityClass, entity, false);

            Object idValue = null;
            List<Field> fields = fieldsWithAnnotation(entityClass, Id.class, true);
            if (fields == null || fields.size() <= 0) {
                throw new Exception("No identity field found in entity");
            }
            Field fieldId = fields.get(0);
            String colName = fieldId.getName();
            Annotation annotation = fieldId.getAnnotation(Column.class);
            if (annotation != null && !((Column) annotation).name().isEmpty()) {
                colName = ((Column) annotation).name();
            }
            idValue = fieldId.get(entity);

            String where = null;
            String[] arguments = null;
            if (idValue != null && !idValue.equals(0)) {
                where = tableName + "." + colName + " = ?";
                arguments = new String[]{idValue.toString()};
            }
            result = (int) getDatabase().update(tableName, values, where, arguments);

            if (!this.database.inTransaction()) {
                this.database.close();
            }

        } catch (Exception e) {
            Log.e(TAG, getCause(e));

        }
        return result;
    }

    private int delete(Class<?> entityClass, Object entity, String[] columns, String[] values) throws Exception {
        Assert.assertNotNull(entity);
        Assert.assertNotNull(entityClass);
        String tableName = entityClass.getSimpleName();

        String where = null;
        StringBuilder sb = new StringBuilder();
        if (columns != null) {
            for (String column : columns) {
                sb.append(" ").append(column).append(" =? and");
            }
            where = sb.toString().replaceAll("and$", "");
        }

        int result = getDatabase().delete(tableName, where, values);

        if (!this.database.inTransaction()) {
            this.database.close();
        }
        return result;
    }

    private <T extends Object> List<T> get(Class<T> entityClass, String selection, String[] selectionArgs, String orderBy, String groupBy) {
        List lista = new ArrayList<T>();
        try {

            List<Map<String, Object>> COLUMNS = this.COLUMNAMES;
            if (!entityClass.isAssignableFrom(this.CLAZZ)) {
                COLUMNS = InitEntityColumnVariables(entityClass);
            }
            String[] cols = new String[COLUMNS.size()];
            String colName;
            for (int i = 0; i < COLUMNS.size(); i++) {
                cols[i] = COLUMNS.get(i).get("colName").toString();
            }

            Cursor cursor = getDatabase().query(entityClass.getSimpleName(), cols, selection,
                    selectionArgs, groupBy, null, orderBy, null);

            if (cursor == null) return null;

            Log.d("SQLLITE", cursor.getCount() + " -> COUNT");
            Log.d("SQLLITE", cursor.moveToFirst() + " -> FIRST");

            if (cursor.getCount() > 0 && cursor.moveToFirst()) {
                Object entityAux = null;
                do {
                    try {
                        entityAux = entityClass.newInstance();
                        for (Map cols_ : COLUMNS) {
                            colName = cols_.get("fieldName") == null ?
                                    cols_.get("colName").toString() :
                                    cols_.get("fieldName").toString();
                            fillEntityFieldFromCursor(cursor, entityAux, colName);
                        }
                        lista.add((T) entityAux);
                    } catch (Exception e) {
                        Utils.WriteErrorLog(e.getMessage(), getClass());
                        throw new Exception(e);
                    }
                } while (!cursor.isAfterLast() && cursor.moveToNext());
            }
            cursor.close();
            if (!this.database.inTransaction()) {
                this.database.close();
            }

        } catch (Exception e) {
            Log.w("EntityManager", e.getMessage());
            return null;
        }
        return lista;
    }

    private <T extends Object> List<T> getbyquery(Class<T> entityClass, String sql) {
        Assert.assertNotNull(entityClass);
        Assert.assertNotNull(sql);
        try {

            Cursor c = getDatabase().rawQuery(sql, null);
            if (c != null && c.getCount() > 0) {

                List lista = new ArrayList();
                List<Map<String, Object>> COLUMNS = InitEntityColumnVariables(entityClass);
                c.moveToFirst();
                String colum = null;
                do {
                    Object resultEntity = entityClass.newInstance();
                    for (Map cols_ : COLUMNS) {
                        colum = cols_.get("fieldName") == null ?
                                cols_.get("colName").toString() :
                                cols_.get("fieldName").toString();
                        fillEntityFieldFromCursor(c, resultEntity, colum);
                    }
                    lista.add(resultEntity);
                } while (c.moveToNext());
                return lista;
            }

        } catch (Exception e) {
            Log.e("EntityManager", e.getMessage());
        }
        return null;
    }


    private List<?> getBy(Class<?> clazz, String[] columns, String[] values, String orderBy, String groupBy) {
        if (values == null) return null;

        StringBuilder sb = new StringBuilder();
        for (String col : columns) {
            sb.append(" ").append(col).append(" =? ").append(" and");
        }
        String sql = sb.toString().replaceAll("and$", "");
        List<?> listado = (List<?>) get(clazz, sql, values, orderBy, groupBy);
        if (listado == null || listado.size() == 0) return null;
        return listado;
    }


    private int count(Class<?> clazz, String[] columns, String[] values, String orderBy, String groupBy) {
        int count = 0;
        try {


            String tableName = clazz.getSimpleName();
            StringBuilder sb = new StringBuilder();
            String[] col = columns == null ? null : new String[]{columns[0]};
            String sqlWhere = null;
            if (columns != null) {
                for (String column : columns) {
                    sb.append(" ").append(column).append(" = ? and");
                }
                sqlWhere = sb.toString().replaceAll("and$", "");
            }


            Cursor cursor = getDatabase().query(tableName, col, sqlWhere, values, groupBy, null, orderBy);

            if (cursor == null || cursor.getCount() == 0) return 0;
            count = cursor.getCount();
            cursor.close();

            if (!this.database.inTransaction()) {
                this.database.close();
            }
        } catch (Exception e) {

            Log.e(TAG, getCause(e));

        }
        return count;
    }

    private String SQLCreateEntityStatement(Class<?> clase) throws Exception {

        String fieldName;
        Field field;
        Class<?> type;
        HashMap column = null;
        List<Map<String, Object>> COLUMNS = this.COLUMNAMES;
        if (!this.CLAZZ.isAssignableFrom(clase)) {
            COLUMNS = InitEntityColumnVariables(clase);
        }
        StringBuilder sb = new StringBuilder("Create table " + clase.getSimpleName() + " (");
        for (Map col : COLUMNS) {

            sb.append(" ").append(col.get("colName")).append(" ").append(Sqltype((Class<?>) col.get("dataType")));
            if (col.get("identity") != null && (boolean) col.get("identity")) {
                sb.append(" PRIMARY KEY ");
                if ((boolean) col.get("autoincrement")) {
                    sb.append(" AUTOINCREMENT ");
                }
            }
            if ((boolean) col.get("unique")) {
                sb.append(" UNIQUE ");
            }
            if (!(boolean) col.get("nullable")) {
                sb.append(" NOT NULL ");
            }
            if (col.get("default") != null) {
                sb.append(" DEFAULT " + col.get("default"));
            }
            sb.append(" ,");
        }
        String sql = sb.toString();
        sql = sql.replaceAll(",$", "");
        sql = sql + " )";
        return sql;
    }

    //Other Methods
    public ContentValues getContentValues(Class<?> entityClass, Object entity, boolean Identity) throws Exception {
        ContentValues values_ = new ContentValues();
        List<Map<String, Object>> COLUMNS = this.COLUMNAMES;

        if (!this.CLAZZ.isAssignableFrom(entityClass)) {
            COLUMNS = InitEntityColumnVariables(entityClass);
        }
        for (Map entry : COLUMNS) {
            if (entry.get("identity") != null && ((boolean) entry.get("identity")) && !Identity)
                continue;

            String key = entry.get("fieldName") == null ?
                    entry.get("colName").toString() :
                    entry.get("fieldName").toString();

            String keyDB = entry.get("fieldName") != null ?
                    entry.get("colName").toString() :
                    entry.get("colName").toString();

            Class<?> type = (Class<?>) entry.get("dataType");
            Field field = entityClass.getDeclaredField(key);
            Object value = null;
            if (!field.isAccessible()) {
                field.setAccessible(true);
            }
            Annotation col = field.getAnnotation(Column.class);
            value = field.get(entity);
            if (value == null && col != null &&
                    !Utils.isEmpty(((Column) col).default_value())) {
                value = ((Column) col).default_value();
            }
            castValue(values_, keyDB, value, type, field);
        }
        return values_;

    }

    private void castValue(ContentValues values_, String key, Object value, Class<?> type, Field f) {
        DateType format = f.getAnnotation(DateType.class);
        String formatDate = format == null ? EntityManager.DATEFORMATNOTIME : format.format();
        if (value == null) return;
        if (type.isAssignableFrom(Integer.class) || type.isAssignableFrom(int.class)) {
            values_.put(key, ((Number) value).intValue());
        } else if (type.isAssignableFrom(Float.class) || type.isAssignableFrom(float.class)) {
            values_.put(key, ((Number) value).floatValue());
        } else if (type.isAssignableFrom(Long.class) || type.isAssignableFrom(long.class)) {
            values_.put(key, ((Number) value).longValue());
        } else if (type.isAssignableFrom(Double.class) || type.isAssignableFrom(double.class)) {
            values_.put(key, ((Number) value).doubleValue());
        } else if (type.isAssignableFrom(String.class)) {
            values_.put(key, (String) value);
        } else if (type.isAssignableFrom(Date.class)) {
            values_.put(key, new SimpleDateFormat(formatDate).format(value));
        }
    }

    private List<Map<String, Object>> InitEntityColumnVariables(Class<?> clazz) throws Exception {
        if (clazz == null) throw new Exception("Class instance not specified.");
        List<Map<String, Object>> COLUMNS = new ArrayList<>();
        String fieldName;
        Class<?> type;
        HashMap column = null;
        boolean isClassField = false;

        for (Field f : clazz.getDeclaredFields()) {
            isClassField = isClassField(f, clazz);

            if (!isClassField) {
                continue;
            }

            if (COLUMNS == null) {
                COLUMNS = new ArrayList<>();
            }
            column = new HashMap();
            applyProperties(f, clazz, column);


            COLUMNS.add(column);
        }
        return COLUMNS;
    }

    private void applyProperties(Field f, Class<?> clazz, Map column) {
        Column columna = null;
        Id id = null;
        ManyToOne manytoone;
        OneToMany onetomany;
        boolean isClassField;

        columna = f.getAnnotation(Column.class);
        id = f.getAnnotation(Id.class);
        manytoone = f.getAnnotation(ManyToOne.class);
        onetomany = f.getAnnotation(OneToMany.class);


        column.put("nullable", true);
        column.put("unique", false);
        column.put("isEntity", false);
        column.put("colName", f.getName());
        column.put("fieldName", f.getName());
        column.put("dataType", f.getType());

        if (columna != null) {

            column.put("nullable", columna.nullable());
            column.put("unique", columna.unique());
            column.put("isEntity", false);

            column.put("fieldName", f.getName());
            if (!columna.name().isEmpty()) {
                column.put("colName", columna.name());
            }

            if (!columna.default_value().isEmpty()) {
                column.put("default", columna.default_value());
            }

        }

        if (id != null) {
            column.put("identity", true);
            column.put("autoincrement", id.autoIncrement());
            column.put("nullable", false);
            column.put("unique", true);
        }

    }


    private boolean isClassField(Field f, Class<?> clazz) {
        String fieldName = (f.getName().substring(0, 1).toUpperCase() + f.getName().substring(1));
        try {
            Method setter = clazz.getMethod("set" + fieldName, f.getType());
            if (setter != null) return true;
        } catch (NoSuchMethodException e) {
        }
        return false;
    }

    private void fillEntityFieldFromCursor(Cursor c, Object entity, String column) throws Exception {
        int positionofcolumn = c.getColumnIndex(column);

        if (Cursor.FIELD_TYPE_NULL == c.getType(positionofcolumn)) {
            return;
        }

        Field f = entity.getClass().getDeclaredField(column);
        DateType format = f.getAnnotation(DateType.class);
        String formatDate = format == null ? EntityManager.DATEFORMATNOTIME : format.format();
        if (!f.isAccessible()) {
            f.setAccessible(true);
        }
        if (Cursor.FIELD_TYPE_INTEGER == c.getType(positionofcolumn)) {
            f.set(entity, (Integer) c.getInt(positionofcolumn));
        } else if (Cursor.FIELD_TYPE_STRING == c.getType(positionofcolumn)) {
            if (f.getType().isAssignableFrom(Date.class)) {
                String date = c.getString(positionofcolumn);
                f.set(entity, new SimpleDateFormat(formatDate).parse(date));
                return;
            }
            f.set(entity, (String) c.getString(positionofcolumn));
        } else if (Cursor.FIELD_TYPE_FLOAT == c.getType(positionofcolumn)) {
            f.set(entity, (Double) c.getDouble(positionofcolumn));
        }
    }

    public String Sqltype(Class<?> type_) {
        if (type_.isAssignableFrom(Integer.class) || type_.isAssignableFrom(int.class)) {
            return " INTEGER ";
        } else if (type_.isAssignableFrom(String.class) || type_.isAssignableFrom(Date.class)) {
            return " TEXT ";
        } else if (type_.isAssignableFrom(Boolean.class)) {
            return " NUMERIC ";
        } else {
            return " REAL ";
        }
    }

    public List<Field> fieldsWithAnnotation(Class<?> clazz, Class anotation, boolean setAccesibleifNotAccesible) {
        List<Field> result = new ArrayList<>();
        for (Field f : clazz.getDeclaredFields()) {
            if (f.getAnnotation(anotation) != null) {
                if (setAccesibleifNotAccesible && !f.isAccessible())
                    f.setAccessible(true);
                result.add(f);
            }
        }
        return result;
    }

    private String getCause(Exception e) {
        return e.getCause() != null ? e.getCause().getMessage() : e.getMessage();
    }


}