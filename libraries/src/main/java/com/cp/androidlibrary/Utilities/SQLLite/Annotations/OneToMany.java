package com.cp.androidlibrary.Utilities.SQLLite.Annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by CarlosP on 8/8/2017.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target( value = { ElementType.FIELD ,ElementType.METHOD } )
public @interface OneToMany {
    public String mappedField();
}
