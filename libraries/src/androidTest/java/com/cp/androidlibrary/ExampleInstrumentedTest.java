package com.cp.androidlibrary;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.cp.androidlibrary.Utilities.Http.handlers.HttpAbstractResult;
import com.cp.androidlibrary.Utilities.Http.headers.HttpHeaders;
import com.cp.androidlibrary.Utilities.Http.request.HttpManager;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.

        isConnected();

    }

    public static boolean isConnected() {
        final boolean[] isConnected = {false};
        HttpManager http = new HttpManager("http://www.google.com.co");
        http.get(new HttpAbstractResult<String>() {
            @Override
            public void onError(Exception excp, HttpHeaders responseHeaders) {
                super.onError(excp, responseHeaders);
            }

            @Override
            public void onSuccess(String resultValue, HttpHeaders responseHeaders, int responseCode) {
                isConnected[0] = true;
            }
        });
        return isConnected[0];
    }
}
