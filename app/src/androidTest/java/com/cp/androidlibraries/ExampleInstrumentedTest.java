package com.cp.androidlibraries;

import android.content.Context;
import android.provider.Settings;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.cp.androidlibrary.Utilities.Http.converter.HttpManagerConverterFactory;
import com.cp.androidlibrary.Utilities.Http.converter.IJSONObjectConverter;
import com.cp.androidlibrary.Utilities.Http.handlers.HttpAbstractResult;
import com.cp.androidlibrary.Utilities.Http.headers.HttpHeaders;


import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.

//        HttpManagerConverterFactory.setCustomConverter((com.cp.androidlibrary.Utilities.Http.converter.IJSONObjectConverter) new JacksonJSONConverter());


        // new com.cp.androidlibrary.Utilities.Http.request.HttpManager("http://portal.unisimonbolivar.edu.co/estudiantes/index.php/login/inicio_sesion")
        new com.cp.androidlibrary.Utilities.Http.request.HttpManager("http://192.168.200.22:8085/Clovis/api/bancos/")
                //.response2Type(LoginResponse.class)
                //.setParams("usuario=cpinto6&clave=Carlos.pinto")
                .setTimeOutTime(1000)
                .response2ArrayType(Bancos.class)
                .get(new HttpAbstractResult<List<Bancos>>() {
                    @Override
                    public void onSuccess(List<Bancos> resultValue, HttpHeaders responseHeaders, int responseCode) {
                        Log.d("TEST", resultValue.toString());
                    }

                    @Override
                    public void onError(Exception excp, HttpHeaders responseHeaders) {
                        if ( excp != null ){
                            Log.d("TEST" , excp.getMessage());
                        }
                    }
                });


    }



    public static class Bancos{

        private Integer id;
        private String codigoBanco;
        private String nombreBanco;
        private Character estado;
        private String usuario;
        private Date fecha;

        public Bancos() {
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getCodigoBanco() {
            return codigoBanco;
        }

        public void setCodigoBanco(String codigoBanco) {
            this.codigoBanco = codigoBanco;
        }

        public String getNombreBanco() {
            return nombreBanco;
        }

        public void setNombreBanco(String nombreBanco) {
            this.nombreBanco = nombreBanco;
        }

        public Character getEstado() {
            return estado;
        }

        public void setEstado(Character estado) {
            this.estado = estado;
        }

        public String getUsuario() {
            return usuario;
        }

        public void setUsuario(String usuario) {
            this.usuario = usuario;
        }

        public Date getFecha() {
            return fecha;
        }

        public void setFecha(Date fecha) {
            this.fecha = fecha;
        }
    }

//    public static class JacksonJSONConverter implements IJSONObjectConverter {
//        private static String JSONCONVERTERTAG = "JACKSONCONV";
//
//        @Override
//        public <T> List<T> DeserializeArray(Class<T> aClass, String s) {
//
//            try {
//                return new ObjectMapper().readValue(s, new TypeReference<List<T>>() {
//                });
//            } catch (IOException e) {
//                Log.e(JSONCONVERTERTAG, "Error convertiendo objeto a List<T> -> " + e.getMessage());
//                return null;
//            }
//
//        }
//
//        @Override
//        public <T> T DeserializeObject(Class<T> aClass, String s) {
//            try {
//                return new ObjectMapper().readValue(s, aClass);
//            } catch (IOException e) {
//                Log.e(JSONCONVERTERTAG, "Error convertiendo objeto a Class<T> -> " + e.getMessage());
//                return null;
//            }
//        }
//
//        @Override
//        public String SerializeObject(Object o) {
//            try {
//                return new ObjectMapper().writeValueAsString(o);
//            } catch (JsonProcessingException e) {
//                Log.e(JSONCONVERTERTAG, "Error convertiendo objeto a string -> " + e.getMessage());
//                return null;
//            }
//        }
//    }


    public static class LoginResponse {

        private boolean success;
        private String u;
        private String c;
        private String msg;
        private HttpHeaders headers;
        private String userName;
        private String token;

        LoginResponse() {

        }


        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public HttpHeaders getHeaders() {
            return headers;
        }

        public void setHeaders(HttpHeaders headers) {
            this.headers = headers;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getU() {
            return u;
        }

        public void setU(String u) {
            this.u = u;
        }

        public String getC() {
            return c;
        }

        public void setC(String c) {
            this.c = c;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }


}
